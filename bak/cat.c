// my 'cat' implementation
#include <stdio.h>
#include <stdlib.h>

// Sanity check for CLI arguments
int sanity_arg(int arg_c) {
	if (arg_c == 1) {
		return 1;
	}
}

int main(int argc, char *argv[]) {
	// Run sanity checks
	if ((sanity_arg(argc)) == 1) {
		printf("%s", "cat: no argument specified");
		// Exit if argc = 1 (no argument specified)
		exit(1);
	}

	for (int i = 1; i < argc; i++) {
		// Open files for reading
		// Our file pointer arrays indexed at 'i'
		FILE* fptr[argc-1];
		fptr[i] = fopen(argv[i], "r");

		// Check if fopen() returns NULL
		if (fptr[i] == NULL) {
			printf("%s - %s", "cat: file not found", argv[i]);
			return 1;
		}
		else {
		// 'cat' implementation starts here
			char ch;
			while((ch = fgetc(fptr[i])) != EOF)
				printf("%c", ch);
		}
	}
}

