// my 'cat' implementation
#include <stdio.h>
#include <stdlib.h>

int concatenate(FILE *infile) {
	// This cat will print char 1-by-1
	char ch;
	// cat implementation starts here
	while((ch = fgetc(infile)) != EOF)
		printf("%c", ch);

	// Close the files
	fclose(infile);
}

// Sanity check for CLI arguments
int sanity_arg(int argc) {
	if (argc == 1) {
		return argc;
	}
}

FILE* sanity_file(char *fname) {
	
	// argc (fcount) will also the program name
	FILE* fptr;
	fptr = fopen(fname, "r");

	// Check if fopen() returns NULL
	if (fptr == NULL) {
		printf("%s - %s\n", "cat: file not found", fname);
		return fptr;
	}
	else {
		return fptr;
	}
}

int main(int argc, char *argv[]) {
	// Run sanity checks
	if ((sanity_arg(argc)) == 1) {
		printf("%s", "cat: no argument specified\n");
		// Exit if argc = 1 (no argument specified)
		exit(1);
	}
	else {
		for (int i = 1; i < argc; i++)
			if (sanity_file(argv[i]) != NULL)
				concatenate(sanity_file(argv[i]));
	}
}
