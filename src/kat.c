// kat - my n00b version of cat. At least it runs faster than cat.
#include <stdio.h>
#include <stdlib.h>

// Sanity check for CLI argument. It takes one argument 'c' (count - from argc) and return c if c = 1. main() then complains 'no argument specified' if c (1) is returned.
int sanity_a(int c) {
	if (c == 1) {
		return c;
	}
}

// Sanity check for files. Returns the file pointer 'fptr'.
FILE* sanity_f(char *fname) {
	
	// Open file(s) for read. fptr will later be closed (if opened) in conkat()
	FILE* fptr;
	fptr = fopen(fname, "r");

	// Check if fopen() returns NULL, and print error messege
	if (fptr == NULL) {
		printf("%s - %s\n", "kat: file not found", fname);
	}
	
	// Returns file pointer fptr. If fptr is not NULL, main() will pass this file pointer return value to conkat.
	return fptr;
}

void conkat(FILE *infile) {
	char ch;
	// feline implementation starts here
	while((ch = fgetc(infile)) != EOF)
		printf("%c", ch);

	// Close the file(s) only if it is opened.
	fclose(infile);
}

int main(int argc, char *argv[]) {
	if ((sanity_a(argc)) == 1) {
		printf("%s", "kat: no argument specified\n");
		// Exit if argc = 1 (no argument specified)
		exit(1);
	}
	else {
		// Loop through all of our arguments. main() will store the file pointer fptr returned by sanity_f() in FILE* F
		for (int i = 1; i < argc; i++) {
			FILE* F;
			F = sanity_f(argv[i]);
			if (F != NULL) {
				conkat(F);
			}
		// I intentionally allow kat to loop through all argument(s), so when the argument points to non-existent files, it will still run, and will returns 0. If you need kat to exit when NULL file pointer is found, you can do it by adding an 'else' block below.
		}
	}
}
